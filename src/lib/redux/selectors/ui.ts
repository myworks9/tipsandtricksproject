// core
import { RootState } from '../init/store';

export const errorMessageSelector = (state: RootState): string => state.ui.errorMessage;
export const isFetchingSelector = (state: RootState): boolean => state.ui.isFetching;

