// core
import { RootState } from '../init/store';

export const isSettingsOpenSelector = (state: RootState): boolean => state.settings.isSettingsOpen;

