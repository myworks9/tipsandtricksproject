// core
import { RootState } from '../init/store';

// types
import { TagModelResponseType } from '../../../types';

export const selectedTagSelector = (
    state: RootState,
): TagModelResponseType => state.tags.selectedTag;

