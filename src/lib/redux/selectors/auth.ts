// core
import { RootState } from '../init/store';

// types
import { ProfileRequestType, ProfileResponseType } from '../../../types';

export const authTokenSelector = (state: RootState): string  => state.auth.authToken;
export const userInfoSelector = (state: RootState): ProfileResponseType  => state.auth.userInfo;
export const userProfileDataSelector = (
    state: RootState,
): ProfileRequestType => state.auth.userProfileData;
