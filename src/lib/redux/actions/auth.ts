// types
import {
    GetProfileActionType,
    ProfileRequestType,
    ProfileResponseType,
    SetAuthTokenActionType,
    SetProfileDataActionType,
} from '../../../types';
import { authTypes } from '../types';

type AuthActionsTypes = {
    setAuthToken: (payload: string | null) => SetAuthTokenActionType
    getProfile: (payload: ProfileResponseType | null) => GetProfileActionType
    setProfileData: (payload: ProfileRequestType | null) => SetProfileDataActionType
};

export const authActions: AuthActionsTypes = {
    setAuthToken:   (payload) => ({ type: authTypes.SET_AUTH_TOKEN, payload }),
    getProfile:     (payload) => ({ type: authTypes.GET_PROFILE, payload }),
    setProfileData: (payload) => ({ type: authTypes.SET_PROFILE_DATA, payload }),
} as const;
