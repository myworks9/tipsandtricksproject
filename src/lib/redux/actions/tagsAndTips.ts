// types
import { SetSelectedTagActionType, TagModelResponseType } from '../../../types';
import { tagsAndTipsTypes } from '../types';

type TagsActionsTypes = {
    setSelectedTag: (payload: TagModelResponseType | null) => SetSelectedTagActionType
};

export const tagsAndTipsActions: TagsActionsTypes = {
    setSelectedTag: (payload) => ({ type: tagsAndTipsTypes.SET_SELECTED_TAG, payload }),
} as const;

