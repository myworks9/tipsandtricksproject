// types
import { setSettingsOpenActionType } from '../../../types';
import { settingsTypes } from '../types';

type settingsActionsTypes = {
    setSettingsOpen: (payload: boolean) => setSettingsOpenActionType
};

export const settingsActions: settingsActionsTypes = {
    setSettingsOpen: (payload) => ({ type: settingsTypes.SET_SETTINGS_OPEN, payload }),
} as const;
