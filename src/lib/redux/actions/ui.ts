// types
import { SetErrorMessageActionType, SetIsFetchingActionType } from '../../../types';
import { uiTypes } from '../types';

type UIActionsTypes = {
    setErrorMessage: (payload: string | null) => SetErrorMessageActionType
    setIsFetching: (payload: boolean) => SetIsFetchingActionType
};

export const uiActions: UIActionsTypes = {
    setErrorMessage: (payload) => ({ type: uiTypes.SET_ERROR_MESSAGE, payload }),
    setIsFetching:   (payload) => ({ type: uiTypes.SET_IS_FETCHING, payload }),
} as const;
