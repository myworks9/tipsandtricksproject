// core
import { NavigateFunction } from 'react-router-dom';
import { AppThunk } from '../init/store';

// actions
import { authActions, uiActions } from '../actions';

// types
import { ProfileResponseType, ToastOptions } from '../../../types';

// api
import { api } from '../../../API';

// helpers & constants
import { createToasters } from '../../../helpers';
import { toastConstants, toastMessages } from '../../../constants';
import { book } from '../../../book';

export const getProfileThunk = (navigate: NavigateFunction): AppThunk => async (dispatch) => {
    try {
        dispatch(uiActions.setIsFetching(true));
        const userInfo: ProfileResponseType = await api.getProfile();

        if (userInfo) {
            createToasters(ToastOptions.success, toastMessages({
                name:    toastConstants.SUCCESS_AUTHORIZATION,
                payload: userInfo?.name,
            }));
            navigate(book.allPages);
        }

        dispatch(authActions.getProfile(userInfo));
    } finally {
        dispatch(uiActions.setIsFetching(false));
    }
};

