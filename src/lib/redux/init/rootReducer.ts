// core
import { combineReducers } from 'redux';

// reducers
import {
    authReducer, settingsReducer, tagsAndTipsReducer, uiReducer,
} from '../reducers';

export const rootReducer = combineReducers({
    auth:     authReducer,
    settings: settingsReducer,
    tags:     tagsAndTipsReducer,
    ui:       uiReducer,
});
