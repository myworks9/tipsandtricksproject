// Core
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { AnyAction, applyMiddleware, createStore } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

// Instruments
import { composeEnhancers, middleware } from './middleware';

import { rootReducer } from './rootReducer';

const persistConfig = {
    key:       'root',
    storage,
    whitelist: ['auth'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);


export const store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(...middleware)),
);

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export type Action = { type: string; payload?: unknown; error?: boolean };
export type AppThunk<ReturnType = void> = ThunkAction<
ReturnType,
RootState,
unknown,
AnyAction
>;
export type TDispatch = ThunkDispatch<RootState, void, AnyAction>;
export type AppDispatch = TDispatch;

