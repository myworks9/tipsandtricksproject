export const enum tagsAndTipsTypes {
    SET_SELECTED_TAG = 'SET_SELECTED_TAG',
    SET_TIPS_META_DATA = 'SET_TIPS_META_DATA',
}
