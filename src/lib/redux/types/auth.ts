export const enum authTypes {
    SET_AUTH_TOKEN = 'SET_AUTH_TOKEN',
    GET_PROFILE = 'GET_PROFILE',
    SET_PROFILE_DATA = 'SET_PROFILE_DATA',
}
