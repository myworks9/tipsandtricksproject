// types
import { SetErrorMessageActionType, SetIsFetchingActionType } from '../../../types';
import { uiTypes } from '../types';

type InitialStateTypes = {
    errorMessage: string | null
    isFetching: boolean
};

const initialState: InitialStateTypes = {
    errorMessage: null,
    isFetching:   false,
};

type UIReducerActionTypes = SetIsFetchingActionType | SetErrorMessageActionType;

export const uiReducer = (state = initialState, action: UIReducerActionTypes) => {
    switch (action.type) {
        case uiTypes.SET_ERROR_MESSAGE: {
            return {
                ...state,
                errorMessage: action.payload,
            };
        }
        case uiTypes.SET_IS_FETCHING: {
            return {
                ...state,
                isFetching: action.payload,
            };
        }
        default: return state;
    }
};
