// types
import {
    GetProfileActionType,
    ProfileRequestType,
    ProfileResponseType,
    SetAuthTokenActionType,
    SetProfileDataActionType,
} from '../../../types';
import { authTypes } from '../types';

type InitialStateType = {
    authToken: string | null,
    userInfo: ProfileResponseType | null,
    userProfileData: ProfileRequestType | {},
};

const initialState: InitialStateType = {
    authToken:       null,
    userInfo:        null,
    userProfileData: {},
};

type AuthReducerActionsTypes =
| SetAuthTokenActionType
| GetProfileActionType
| SetProfileDataActionType;

export const authReducer = (state = initialState, action: AuthReducerActionsTypes) => {
    switch (action.type) {
        case authTypes.SET_AUTH_TOKEN: {
            return {
                ...state,
                authToken: action.payload,
            };
        }
        case authTypes.GET_PROFILE: {
            return {
                ...state,
                userInfo: action.payload,
            };
        }
        case authTypes.SET_PROFILE_DATA: {
            return {
                ...state,
                userProfileData: action.payload ? {
                    ...state.userProfileData,
                    ...action.payload,
                } : {},
            };
        }
        default: return state;
    }
};
