// types
import { tagsAndTipsTypes } from '../types';
import { SetSelectedTagActionType, TagModelResponseType } from '../../../types';

type InitialStateType = {
    selectedTag: TagModelResponseType | null
};

const initialState: InitialStateType = {
    selectedTag: null,
};

type TagsReducerActionType = SetSelectedTagActionType;

export const tagsAndTipsReducer = (state = initialState, action: TagsReducerActionType) => {
    switch (action.type) {
        case tagsAndTipsTypes.SET_SELECTED_TAG: {
            return {
                ...state,
                selectedTag: action.payload,
            };
        }
        default: return state;
    }
};
