// types
import { setSettingsOpenActionType } from '../../../types';
import { settingsTypes } from '../types';

type InitialStateType = {
    isSettingsOpen: boolean
};

const initialState: InitialStateType = {
    isSettingsOpen: false,
};

type SettingsReducerActionsType = setSettingsOpenActionType;

export const settingsReducer = (state = initialState, action: SettingsReducerActionsType) => {
    switch (action.type) {
        case settingsTypes.SET_SETTINGS_OPEN: {
            return {
                ...state,
                isSettingsOpen: action.payload,
            };
        }
        default: return state;
    }
};
