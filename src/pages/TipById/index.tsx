// core
import { FC } from 'react';

// hooks
import { useParams } from 'react-router-dom';
import { useTipById } from '../../hooks/useTipById';

// types
import { ViewPage } from '../../types';

// components
import { TipByIdAside, TipByIdHero, TipByIdMain } from '../../components';

export const TipById: FC = () => {
    const { id } = useParams();
    const { tip, isFetched } = useTipById(id);

    return (
        <section className = 'tip-view-layout'>
            <TipByIdHero
                title = { tip?.title }
                tagImg = { tip?.tag?.name }
                author = { tip?.author }
                created = { tip?.created }
                isFetched = { isFetched } />
            <TipByIdMain body = { tip?.body } isFetched = { isFetched } />
            <TipByIdAside viewPage = { ViewPage.BY_TAG_PAGE } />
        </section>
    );
};
