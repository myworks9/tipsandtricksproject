// core
import { FC, ReactNode } from 'react';

type PropTypes = {
    children: ReactNode
};

export const ProfilePage: FC<PropTypes> = ({ children }) => {
    return (
        <section className = 'profile'>
            <section className = 'publish-tip'>
                { children }
            </section>
        </section>
    );
};
