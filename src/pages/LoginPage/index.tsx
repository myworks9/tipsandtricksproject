// core
import { FC } from 'react';

// components
import { LoginForm } from '../../components';

export const LoginPage: FC = () => {
    return (
        <section className = 'login'>
            <LoginForm />
        </section>
    );
};
