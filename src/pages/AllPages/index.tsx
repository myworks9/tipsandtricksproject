// core
import { FC } from 'react';

// types
import { ViewPage } from '../../types';

// components
import { Hero, TipList } from '../../components';

export const AllPages: FC = () => {
    return (
        <>
            <Hero viewPage = { ViewPage.ALL_TIPS }>
                <h2>All Topics Page</h2>
            </Hero>
            <TipList viewPage = { ViewPage.ALL_TIPS } />
        </>
    );
};

