// core
import { FC } from 'react';

// components
import { SignUpForm } from '../../components';

export const SignUpPage: FC = () => {
    return (
        <section className = 'signup'>
            <SignUpForm />
        </section>
    );
};
