// Core
import { FC } from 'react';

// hooks
import { useAppSelector } from '../../lib/redux/init/store';

// selectors
import { selectedTagSelector } from '../../lib/redux/selectors';

// components
import { Hero, TipList } from '../../components';
import { ViewPage } from '../../types';

export const ByTagPage: FC = () => {
    const selectedTag = useAppSelector(selectedTagSelector);

    return (
        <>
            <Hero viewPage = { ViewPage.BY_TAG_PAGE }>
                <h2>{ selectedTag?.name }</h2>
            </Hero>
            <TipList viewPage = { ViewPage.BY_TAG_PAGE } />
        </>
    );
};
