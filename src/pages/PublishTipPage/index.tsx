// core
import { FC } from 'react';

// components
import { PublishTipForm } from '../../components';

export const PublishTipPage: FC = () => {
    return (
        <section className = 'publish-tip'>
            <PublishTipForm />
        </section>
    );
};
