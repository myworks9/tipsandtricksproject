// core
import { NavigateFunction } from 'react-router-dom';
import { AppDispatch } from '../lib/redux/init/store';

// thunks
import { getProfileThunk } from '../lib/redux/thunk';

// actions
import { authActions } from '../lib/redux/actions';

// types
import { SetOrResetAuthData } from '../types';

// API
import { rootAPI } from '../API';

type SetOrResetAuthDataType = <O extends SetOrResetAuthData>(
    dispatch: AppDispatch,
    navigate: NavigateFunction,
    setOrReset: O,
    token?: string
) => void;

export const setOrResetAuthData: SetOrResetAuthDataType = (
    dispatch, navigate, setOrReset, token,
) => {
    if (setOrReset === SetOrResetAuthData.SET_AUTH_DATA) {
        localStorage.setItem('token', token);
        dispatch(authActions.setAuthToken(token));
        rootAPI.defaults.headers.common.Authorization = `Bearer ${token}`;
        dispatch(getProfileThunk(navigate));

        return;
    }
    localStorage.removeItem('token');
    dispatch(authActions.setAuthToken(null));
    delete rootAPI.defaults.headers.common.Authorization;
};
