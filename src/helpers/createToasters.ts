// core
import { toast, Id } from 'react-toastify';

// types
import { ToastOptions } from '../types';

// constants
import { toastOptions, toastConstants } from '../constants';

export type CreateToastersType = <O extends ToastOptions>(
    typeOfToasts: O,
    message: typeof toastConstants | string
) => Id;

export const createToasters: CreateToastersType = (typeOfToast, message): string | number => {
    return toast[ typeOfToast ](message, toastOptions);
};

