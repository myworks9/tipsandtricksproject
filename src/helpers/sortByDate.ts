// types
import { TipModelResponseType } from '../types';

export const sortByDate = (a: TipModelResponseType, b: TipModelResponseType): number => {
    return new Date(b.created).getTime() - new Date(a.created).getTime();
};

