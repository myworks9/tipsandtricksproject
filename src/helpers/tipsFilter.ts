// types
import {
    TagModelResponseType,
    TipModelResponseType,
    TipsAndMetaDataResponseType,
    ViewPage,
} from '../types';

type FilterTipsArgTypes = {
    tips: TipsAndMetaDataResponseType[],
    viewPage: ViewPage,
    selectedTag: TagModelResponseType
};

export const tipsFilter = ({ tips, viewPage, selectedTag }:FilterTipsArgTypes) => {
    let filteredTips: TipModelResponseType[] = [];

    if (viewPage === ViewPage.BY_TAG_PAGE) {
        tips.forEach((t) => {
            filteredTips = t.data.filter((tip) => tip.tag.id === selectedTag.id);
        });

        return filteredTips;
    }

    tips.forEach((t) => {
        t.data.forEach((tip) => filteredTips.push(tip));
    });

    return filteredTips;
};
