// core
import { ReactElement } from 'react';

type ContentType = string | undefined | null | ReactElement[];

export const fetchify = (isFetched: boolean, content: ContentType): string | null | ContentType => {
    if (!isFetched && !content) {
        return 'Загрузка...';
    }

    if (typeof content !== 'undefined') {
        return content;
    }

    return null;
};

