// types
import { uiTypes } from '../../lib/redux/types';

export type SetIsFetchingActionType = {
    type:    uiTypes.SET_IS_FETCHING,
    payload: boolean
};

export type SetErrorMessageActionType = {
    type:    uiTypes.SET_ERROR_MESSAGE,
    payload: string | null
};
