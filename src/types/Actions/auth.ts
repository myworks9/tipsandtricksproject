// types
import { authTypes } from '../../lib/redux/types';
import { ProfileRequestType, ProfileResponseType } from '../API';

export type SetAuthTokenActionType = {
    type:    authTypes.SET_AUTH_TOKEN,
    payload: string | null,
};

export type GetProfileActionType = {
    type:    authTypes.GET_PROFILE,
    payload: null | ProfileResponseType,
};

export type SetProfileDataActionType = {
    type:    authTypes.SET_PROFILE_DATA,
    payload: null | ProfileRequestType,
};
