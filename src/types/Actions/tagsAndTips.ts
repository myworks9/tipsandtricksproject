// types
import { tagsAndTipsTypes } from '../../lib/redux/types';
import { TagModelResponseType } from '../API';

export type SetSelectedTagActionType = {
    type:    tagsAndTipsTypes.SET_SELECTED_TAG,
    payload: TagModelResponseType | null,
};
