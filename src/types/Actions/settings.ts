// types
import { settingsTypes } from '../../lib/redux/types';

export type setSettingsOpenActionType = {
    type:    settingsTypes.SET_SETTINGS_OPEN,
    payload: boolean,
};
