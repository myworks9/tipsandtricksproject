//* ViewPage *//
export type ViewPageType = 'by-tip-page' | 'all-tips';

//* ToastOptions *//
export const enum ToastOptions {
    info = 'info',
    success = 'success',
    warning = 'warning',
    error = 'error',
}

//* SetOrResetAuthData *//
export const enum SetOrResetAuthData {
    SET_AUTH_DATA = 'SET_AUTH_DATA',
    RESET_AUTH_DATA = 'RESET_AUTH_DATA',
}

//* viewPage *//
export const enum ViewPage {
    ALL_TIPS = 'ALL_TIPS',
    BY_TAG_PAGE = 'BY_TAG_PAGE',
}
