// core
import { FC, SyntheticEvent } from 'react';

export type CustomLinkTypes = {
    path: string,
    disableLinkHandler?: (event: SyntheticEvent<HTMLAnchorElement>) => void,
    openSettingsHandler?: (event: SyntheticEvent<HTMLAnchorElement>) => void,
    icon: FC,
    text: string
};
