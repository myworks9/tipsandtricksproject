//* Registration *//
export type SignUpResponseType = {
    name: string,
    email: string,
    token: string,
};

export type SignUpFormShapeType = {
    name: string,
    email: string,
    password: string,
    confirmPassword: string,
};

export type SignUpFormRequestType = Omit<SignUpFormShapeType, 'confirmPassword'>;

//* Login *//
export type LoginResponseType = {
    data: string;
};

export type LoginFormRequestType = {
    email: string,
    password: string,
};

//* Profile *//
export type ProfileResponseType = {
    id: string,
    name: string,
    email: string,
    created: Date,
};

export type ProfileRequestType = {
    name?: string,
    email?: string,
    phone?: string,
    job?: string,
    linkedin?: string,
};

export type ProfileStepOneRequestType = Omit<ProfileRequestType, 'job' | 'linkedin'>;

export type ProfileStepTwoRequestType = Omit<ProfileRequestType, 'name' | 'email' | 'phone'>;

export type ProfileStepThreeRequestType = {
    password: string,
    confirmPassword: string,
};

