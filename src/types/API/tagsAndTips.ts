//* Tags *//
export const enum TagModelEnum {
    JAVASCRIPT = 'JavaScript',
    TYPESCRIPT = 'TypeScript',
    REACT = 'React',
    VSCODE = 'VSCode',
    NEXT_JS = 'Next.js',
    NODE_JS = 'Node.js',
    CSS = 'CSS',
    UI_UX = 'UI/UX',
    GRAPHQL = 'GraphQL',
    TESTING = 'Testing',
    NPM = 'Npm',
    GIT = 'Git',
    TOOLS = 'Tools',
    MACOS = 'macOS',
}

export type TagModelResponseType = {
    id: string,
    name: TagModelEnum,
};

//* Tips *//
export type TipModelResponseType = {
    id: string;
    title: string;
    preview: string;
    body: string;
    author: string;
    tag: {
        id: string;
        name: string;
    };
    created: string;
};

export type TipModelFormRequestType = {
    title: string,
    preview: string,
    body: string,
    tagId: string,
};

export type TipMetaDataResponseType = {
    hasNextPage: boolean,
    hasPrevPage: boolean,
    limit: number
    nextPage: number | null
    prevPage: number | null
    pagingCounter: number
    totalDocs: number
    totalPages: number
};

export type TipsAndMetaDataResponseType = {
    data: TipModelResponseType[],
    meta: TipMetaDataResponseType
};

