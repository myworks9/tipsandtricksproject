// core
import axios from 'axios';

// API
import { authAPI } from './authAPI';
import { tagsAPI } from './tagsAPI';
import { tipsAPI } from './tipsAPI';

export const rootAPI = axios.create({
    baseURL: 'https://lab.lectrum.io/rtx/api/tips-and-tricks',
});

export const api = {
    ...tipsAPI,
    ...authAPI,
    ...tagsAPI,
} as const;
