// core
import { AxiosResponse } from 'axios';

// types
import {
    LoginFormRequestType,
    LoginResponseType,
    ProfileResponseType,
    SignUpFormRequestType,
    SignUpResponseType,
} from '../types';

// API
import { rootAPI } from './api';

export const authAPI = {
    async login(credentials: LoginFormRequestType): Promise<LoginResponseType> {
        const { email, password } = credentials;
        const { data } = await rootAPI.get<LoginResponseType>('/auth/login', {
            headers: {
                Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
            },
        });

        return data;
    },
    async signUp(userData: SignUpFormRequestType): Promise<SignUpResponseType> {
        const { data } = await rootAPI.post<SignUpFormRequestType, AxiosResponse<SignUpResponseType>>('/auth/registration', userData);

        return data;
    },
    async getProfile(): Promise<ProfileResponseType> {
        const { data } = await rootAPI.get<ProfileResponseType>('/auth/profile');

        return data;
    },
    async logOut(): Promise<void> {
        await rootAPI.get('/auth/logout');
    },
} as const;
