// types
import { TagModelResponseType } from '../types';

// API
import { rootAPI } from './api';

export const tagsAPI = {
    async getTags(): Promise<TagModelResponseType[]> {
        const { data } = await rootAPI.get<TagModelResponseType[]>('/tags');

        return data;
    },
} as const;

