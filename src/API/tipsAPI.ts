// core
import { AxiosResponse } from 'axios';

// actions
import { tagsAndTipsActions } from '../lib/redux/actions';

// types
import { AppDispatch } from '../lib/redux/init/store';
import {
    TipModelFormRequestType,
    TipModelResponseType,
    TipsAndMetaDataResponseType,
    ViewPage,
} from '../types';

// API
import { rootAPI } from './api';

type TipsTypes = {
    pageParam: number,
    viewPage: ViewPage,
};

export const tipsAPI = {
    async getTips({ pageParam = 1, viewPage }: TipsTypes): Promise<TipsAndMetaDataResponseType> {
        const { data } = await rootAPI.get<TipsAndMetaDataResponseType>('/tips', {
            params: {
                page:  viewPage === ViewPage.ALL_TIPS ? pageParam : 1,
                limit: viewPage === ViewPage.ALL_TIPS ? 50 : Number.MAX_SAFE_INTEGER,
            },
        });

        return data;
    },
    async getTipById(tipId: string): Promise<TipModelResponseType> {
        const { data } = await rootAPI.get<TipModelResponseType>(`/tips/${tipId}`);

        return data;
    },
    async postNewTip(tip: TipModelFormRequestType): Promise<TipModelResponseType> {
        const { data } = await rootAPI.post<TipModelFormRequestType, AxiosResponse<TipModelResponseType>>('/tips', tip);

        return data;
    },
} as const;
