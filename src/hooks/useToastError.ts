// hooks
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '../lib/redux/init/store';

// selectors && actions
import { uiActions } from '../lib/redux/actions';
import { errorMessageSelector } from '../lib/redux/selectors';

// types
import { ToastOptions } from '../types';

// helpers
import { createToasters } from '../helpers';

export const useToastError = (): void => {
    const errorMessage = useSelector(errorMessageSelector);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (errorMessage) {
            createToasters(ToastOptions.error, errorMessage);

            dispatch(uiActions.setErrorMessage(null));
        }
    }, [errorMessage]);
};

