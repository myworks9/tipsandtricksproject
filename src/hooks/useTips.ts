/* eslint-disable @typescript-eslint/no-misused-promises */
// hooks
import { useInfiniteQuery } from 'react-query';
import { useAppSelector } from '../lib/redux/init/store';
import { useScroll } from './useScroll';

// selectors
import { selectedTagSelector } from '../lib/redux/selectors';

// types
import { TipModelResponseType, ViewPage } from '../types';

// api
import { api } from '../API';

// helpers
import { tipsFilter } from '../helpers';

type UseTipsReturnType = {
    tips: TipModelResponseType[],
    isFetched: boolean,
    isFetching: boolean
};

export const useTips = (viewPage?: ViewPage): UseTipsReturnType => {
    const selectedTag = useAppSelector(selectedTagSelector);
    let filteredTips: void | TipModelResponseType[] = null;

    const {
        data: tips, isFetched, isFetching, hasNextPage, fetchNextPage,
    } = useInfiniteQuery(
        ['tips'],
        ({ pageParam }) => api.getTips({ pageParam, viewPage }),
        {
            getNextPageParam(lastPage) {
                if (lastPage.meta.hasNextPage) return lastPage.meta.nextPage;

                return null;
            },
        },
    );

    useScroll({
        isFetching, hasNextPage, fetchNextPage, viewPage,
    });

    if (Array.isArray(tips?.pages)) {
        filteredTips = tipsFilter({ tips: tips.pages, viewPage, selectedTag });
    }

    return {
        tips: Array.isArray(filteredTips) ? filteredTips : [],
        isFetched,
        isFetching,
    };
};

