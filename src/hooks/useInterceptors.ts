// core
import { AxiosError } from 'axios';

// hooks
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { useAppDispatch } from '../lib/redux/init/store';

// actions
import { uiActions } from '../lib/redux/actions';

// types
import { SetOrResetAuthData } from '../types';

// helpers
import { setOrResetAuthData } from '../helpers';

// api
import { rootAPI } from '../API';

type AxiosErrorType = {
    statusCode: number,
    message: string,
    error: string
};


export const useInterceptors = (): void => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    useEffect((): void => {
        rootAPI.interceptors.response.use(
            undefined,
            (error: AxiosError<AxiosErrorType>): Promise<AxiosError> => {
                if (error.response) {
                    if (error.response.status === 401) {
                        setOrResetAuthData(
                            dispatch, navigate, SetOrResetAuthData.RESET_AUTH_DATA,
                        );
                    }

                    dispatch(uiActions.setErrorMessage(error.response.data.message));
                }

                return Promise.reject(error);
            },
        );
    }, []);
};
