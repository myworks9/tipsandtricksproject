/* eslint-disable @typescript-eslint/no-misused-promises */
// hooks
import { UIEvent, useEffect } from 'react';

// types
import { FetchNextPageOptions, InfiniteQueryObserverResult } from 'react-query';
import { TipsAndMetaDataResponseType, ViewPage } from '../types';

type UseScrollArgTypes = {
    isFetching: boolean,
    hasNextPage: boolean,
    viewPage: ViewPage,
    fetchNextPage: (options?: FetchNextPageOptions) => Promise<
    InfiniteQueryObserverResult<TipsAndMetaDataResponseType, unknown>
    >
};

export const useScroll = ({
    isFetching, hasNextPage, fetchNextPage, viewPage,
}: UseScrollArgTypes) => {
    useEffect(() => {
        const onScroll = async (event: Event & UIEvent<Document, UIEvent>): Promise<void> => {
            const { scrollHeight, scrollTop, clientHeight } = event.currentTarget.scrollingElement;

            if (!isFetching && hasNextPage && viewPage === ViewPage.ALL_TIPS) {
                if (scrollHeight - scrollTop <= clientHeight * 1.5) {
                    await fetchNextPage();
                }
            }
        };

        document.addEventListener('scroll', onScroll);

        return () => {
            document.removeEventListener('scroll', onScroll);
        };
    }, [hasNextPage]);
};
