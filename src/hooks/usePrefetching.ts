// hooks
import { useQueryClient } from 'react-query';

// types
import { TipModelResponseType } from '../types';

// api
import { api } from '../API';

type UsePrefetchingReturnType = () => Promise<void>;

export const usePrefetching = (keyAndId: string[]): UsePrefetchingReturnType => {
    const client = useQueryClient();
    const [, id] = keyAndId;

    return async () => {
        await client.prefetchQuery(keyAndId, (): Promise<void | TipModelResponseType> => {
            const tipState = client.getQueryState(keyAndId);
            if (Array.isArray(keyAndId) && tipState.status !== 'success') {
                return api.getTipById(id);
            }

            return null;
        });
    };
};

