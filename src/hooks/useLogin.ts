// hooks
import { UseMutationResult, useMutation } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../lib/redux/init/store';

// types
import { LoginFormRequestType, LoginResponseType, SetOrResetAuthData } from '../types';

// api
import { api } from '../API';

// helpers
import { setOrResetAuthData } from '../helpers';

type UseLoginReturnType = UseMutationResult<
LoginResponseType, unknown, LoginFormRequestType, unknown>;

export const useLogin = (): UseLoginReturnType => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const mutation = useMutation(
        'login',
        (credentials: LoginFormRequestType) => api.login(credentials),
        {
            async onSuccess({ data: token }): Promise<void> {
                await setOrResetAuthData(
                    dispatch, navigate, SetOrResetAuthData.SET_AUTH_DATA, token,
                );
            },
        },
    );

    return mutation;
};

