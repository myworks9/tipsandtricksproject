// hooks
import { useNavigate } from 'react-router-dom';
import { ReactElement, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

// selectors
import { authTokenSelector } from '../lib/redux/selectors';

// types
import { SetOrResetAuthData } from '../types';

// helpers
import { book } from '../book';
import { setOrResetAuthData } from '../helpers';


export const useCheckToken = (): ReactElement | void => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const storageToken = useAppSelector(authTokenSelector);
    const token = localStorage.getItem('token');

    useEffect(() => {
        if (!storageToken && !token) {
            return navigate(book.login);
        }

        if (!storageToken && token) {
            setOrResetAuthData(
                dispatch, navigate, SetOrResetAuthData.SET_AUTH_DATA, token,
            );
        }
    }, [token, storageToken]);
};
