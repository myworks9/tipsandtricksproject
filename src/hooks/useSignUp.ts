// hooks
import { UseMutationResult, useMutation } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../lib/redux/init/store';

// types
import { SetOrResetAuthData, SignUpFormRequestType, SignUpResponseType } from '../types';

// api
import { api } from '../API';

// helpers
import { setOrResetAuthData } from '../helpers';

type UseSignUpReturnType = UseMutationResult<
SignUpResponseType, unknown, SignUpFormRequestType, unknown>;

export const useSignUp = (): UseSignUpReturnType => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const mutation = useMutation(
        'sign-up',
        (newUser: SignUpFormRequestType) => api.signUp(newUser),
        {
            async onSuccess({ token }): Promise<void> {
                await setOrResetAuthData(
                    dispatch, navigate, SetOrResetAuthData.SET_AUTH_DATA, token,
                );
            },
        },
    );

    return mutation;
};

