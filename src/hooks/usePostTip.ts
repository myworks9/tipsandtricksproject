// hooks
import { UseMutationResult, useMutation, useQueryClient } from 'react-query';
import { useNavigate } from 'react-router-dom';

// types
import { TipModelFormRequestType, TipModelResponseType } from '../types';

// api
import { api } from '../API';

// book
import { book } from '../book';

type UsePostTipReturnType = UseMutationResult<
TipModelResponseType, unknown, TipModelFormRequestType, unknown>;


export const usePostTip = (): UsePostTipReturnType => {
    const client = useQueryClient();
    const navigate = useNavigate();

    const mutation = useMutation((tip: TipModelFormRequestType) => api.postNewTip(tip), {
        async onSuccess() {
            await client.invalidateQueries(['tips'])
                .then(() => navigate(book.byTagPage));
        },
    });

    return mutation;
};

