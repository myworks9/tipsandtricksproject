// hooks
import { useEffect } from 'react';
import { useQuery } from 'react-query';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

// selectors && actions
import { tagsAndTipsActions } from '../lib/redux/actions';
import { selectedTagSelector } from '../lib/redux/selectors';

// types
import { TagModelResponseType } from '../types';

// api
import { api } from '../API';

type UseTagsReturnType = {
    tags: TagModelResponseType[]
    isFetched: boolean,
    selectedTagInfo: TagModelResponseType
};

export const useTags = (): UseTagsReturnType => {
    const dispatch = useAppDispatch();
    const selectedTagInfo = useAppSelector(selectedTagSelector);

    const { data: tags, isFetched } = useQuery('tags', () => api.getTags());

    useEffect(() => {
        if (Array.isArray(tags) && tags.length > 0 && !selectedTagInfo) {
            dispatch(tagsAndTipsActions.setSelectedTag(tags[ 0 ]));
        }
    }, [tags]);

    return {
        tags: Array.isArray(tags) ? tags : [],
        isFetched,
        selectedTagInfo,
    };
};

