// hooks
import { UseMutateAsyncFunction, useMutation } from 'react-query';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../lib/redux/init/store';

// types
import { SetOrResetAuthData } from '../types';

// api
import { api } from '../API';

// helpers
import { setOrResetAuthData } from '../helpers';

type UseLogOutReturnType = {
    mutateAsync: UseMutateAsyncFunction<void, unknown, void, unknown>
};

export const useLogOut = (): UseLogOutReturnType => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const { mutateAsync } = useMutation(
        ['logOut'],
        () => api.logOut(),
        {
            onSuccess() {
                setOrResetAuthData(dispatch, navigate, SetOrResetAuthData.RESET_AUTH_DATA);
            },
        },
    );

    return {
        mutateAsync,
    };
};
