// hooks
import { useQuery } from 'react-query';

// types
import { TipModelResponseType } from '../types';

// api
import { api } from '../API';

type UseTipByIdReturnType = {
    tip: TipModelResponseType
    isFetched: boolean
};

export const useTipById = (tipId: string): UseTipByIdReturnType => {
    const { data: tip, isFetched } = useQuery(['tip', tipId], () => api.getTipById(tipId));

    return {
        tip,
        isFetched,
    };
};

