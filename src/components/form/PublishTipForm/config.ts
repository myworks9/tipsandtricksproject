// core
import { lorem, system } from 'faker';
import * as yup from 'yup';

// types
import { TipModelFormRequestType } from '../../../types';

// eslint-disable-next-line no-template-curly-in-string
const minLength = 'Минимальная длина ${min}';

// eslint-disable-next-line no-template-curly-in-string
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<TipModelFormRequestType> = yup.object().shape({
    title:   yup.string().min(4, minLength).max(40, maxLength).required('Заголовок об\'язателен'),
    preview: yup.string().min(15, minLength).max(200, maxLength).required('Превью об\'язательно'),
    body:    yup.string().min(200, minLength).max(250, maxLength).required('Описание об\'язательно '),
    tagId:   yup.string().required('Тег об\'язателен'),
});

export const getNewTipPlaceholder = (): TipModelFormRequestType => {
    const version = system.semver();

    return {
        title:   `Повторение мать учений ! v${version}`,
        preview: `Семь раз отмерь - один раз отрежь v${version}`,
        body:    lorem.words(35),
        tagId:   '',
    };
};
