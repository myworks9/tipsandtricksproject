/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC, useEffect } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { usePostTip, useTags } from '../../../hooks';

// types
import { TipModelFormRequestType } from '../../../types';

// components
import { Input } from '../elements';

// other
import { getNewTipPlaceholder, schema } from './config';

export const PublishTipForm: FC = () => {
    const mutation = usePostTip();
    const { tags } = useTags();

    const form = useForm<TipModelFormRequestType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    useEffect(() => {
        const placeHolder = getNewTipPlaceholder();
        form.setValue('title', placeHolder.title);
        form.setValue('preview', placeHolder.preview);
        form.setValue('body', placeHolder.body);
        form.setValue('tagId', placeHolder.tagId);
    }, [tags]);

    const submit = form.handleSubmit(async (tip: TipModelFormRequestType) => {
        await mutation.mutateAsync(tip);
        form.reset();
    });

    return (
        <form onSubmit = { submit }>
            <fieldset>
                <legend>Опубликовать Типс</legend>
                <Input
                    label = 'Заголовок'
                    register = { form.register('title') }
                    error = { form.formState.errors.title } />
                <Input
                    label = 'Превью'
                    register = { form.register('preview') }
                    error = { form.formState.errors.preview } />
                <Input
                    tag = 'textarea'
                    label = 'Описание'
                    register = { form.register('body') }
                    error = { form.formState.errors.body } />
                <Input
                    tag = 'select'
                    label = 'Тег'
                    options = { tags }
                    register = { form.register('tagId') }
                    error = { form.formState.errors.tagId } />
                <input
                    type = 'submit'
                    value = 'Опубликовать' />
            </fieldset>
        </form>
    );
};
