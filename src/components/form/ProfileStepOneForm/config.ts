// core
import * as yup from 'yup';

// types
import { ProfileStepOneRequestType } from '../../../types';

// eslint-disable-next-line no-template-curly-in-string
const minLength = 'Минимальная длина ${min}';

// eslint-disable-next-line no-template-curly-in-string
const maxLength = 'Максимальная длина ${max}';

const phoneRegExp = /^\+[0-9]{3}\s\((\d+)\)-\d{3}-\d{2}-\d{2}/;

export const schema: yup.SchemaOf<ProfileStepOneRequestType> = yup.object().shape({
    name:  yup.string().required('Введите Ваше им\'я пожалуйста').min(6, minLength).max(25, maxLength),
    email: yup.string().required('Введите Ваш email').email(),
    phone: yup.string().matches(phoneRegExp, 'Введите корректный номер. Например +380 (XX)-XXX-XX-XX'),
});
