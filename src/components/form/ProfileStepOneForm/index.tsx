/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector } from '../../../lib/redux/init/store';

// actions & actions
import { userProfileDataSelector } from '../../../lib/redux/selectors';
import { authActions } from '../../../lib/redux/actions';

// types
import { ProfileStepOneRequestType } from '../../../types';

// components
import { ProfilePage } from '../../../pages';
import { Input } from '../elements';
import { ProfileFormButtons } from '../ProfileFormButtons';

// schema
import { schema } from './config';
import { book } from '../../../book';

export const ProfileStepOneForm: FC = () => {
    const dispatch = useAppDispatch();
    const userProfileData = useAppSelector(userProfileDataSelector);

    const { register, formState, getValues } = useForm<ProfileStepOneRequestType>({
        mode:          'onChange',
        resolver:      yupResolver(schema),
        defaultValues: {
            name:  userProfileData.name || '',
            email: userProfileData.email || '',
            phone: userProfileData.phone || '',
        },
    });

    const submit = () => {
        const values = getValues();
        dispatch(authActions.setProfileData(values));
    };

    return (
        <ProfilePage>
            <form>
                <fieldset>
                    <legend>Ваши контактные данные</legend>
                    <Input
                        register = { register('name') }
                        label = 'Имя и фамилия'
                        error = { formState.errors.name } />
                    <Input
                        type = 'email'
                        register = { register('email') }
                        label = 'Электропочта'
                        error = { formState.errors.email } />
                    <Input
                        type = 'phone'
                        register = { register('phone') }
                        label = 'Телефон'
                        error = { formState.errors.phone } />
                    <ProfileFormButtons
                        submit = { submit }
                        navigateTo = { { next: book.profileStepTwo } }
                        isDisabled = { true }
                        isValid = { formState.isValid } />
                </fieldset>
            </form>
        </ProfilePage>
    );
};
