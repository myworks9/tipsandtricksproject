// core
import * as yup from 'yup';

// types
import { ProfileStepThreeRequestType } from '../../../types';

// eslint-disable-next-line no-template-curly-in-string
const minLength = 'Минимальная длина ${min}';

// eslint-disable-next-line no-template-curly-in-string
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<ProfileStepThreeRequestType> = yup.object().shape({
    password:        yup.string().required('Введите Ваше пароль пожалуйста').min(6, minLength).max(12, maxLength),
    confirmPassword: yup.string().required('Повторите Ваш пароль').oneOf([yup.ref('password')], 'Пароли не совпадают'),
});
