/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

// types
import { ProfileStepThreeRequestType, ToastOptions } from '../../../types';

// components
import { Input } from '../elements';
import { ProfilePage } from '../../../pages';

// schema & helpers
import { schema } from './config';
import { createToasters } from '../../../helpers';
import { toastConstants, toastMessages } from '../../../constants';
import { book } from '../../../book';

export const ProfileStepThreeForm: FC = () => {
    const navigate = useNavigate();

    const { register, formState, handleSubmit } = useForm<ProfileStepThreeRequestType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const submit = handleSubmit((): void => {
        createToasters(ToastOptions.success, toastMessages({
            name: toastConstants.CHANGE_PROFILE_DATA,
        }));
        navigate(book.allPages);
    });

    return (
        <ProfilePage >
            <form onSubmit = { submit }>
                <fieldset>
                    <legend>Смена пароля</legend>
                    <Input
                        type = 'password'
                        register = { register('password') }
                        label = 'Пароль'
                        error = { formState.errors.password } />
                    <Input
                        type = 'password'
                        register = { register('confirmPassword') }
                        label = 'Подтверждение пароля'
                        error = { formState.errors.confirmPassword } />
                    <input
                        type = 'submit'
                        value = 'Обновить профиль' />
                </fieldset>
            </form>
        </ProfilePage>
    );
};
