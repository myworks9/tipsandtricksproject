// core
import * as yup from 'yup';

// types
import { SignUpFormShapeType } from '../../../types';

// eslint-disable-next-line no-template-curly-in-string
const minLength = 'Минимальная длина ${min}';

// eslint-disable-next-line no-template-curly-in-string
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<SignUpFormShapeType> = yup.object().shape({
    name:            yup.string().required('*').min(5, minLength).max(20, maxLength),
    email:           yup.string().required('*').email('Введите email корректный'),
    password:        yup.string().required('*').min(5, minLength).max(12, maxLength),
    confirmPassword: yup.string().required('*').oneOf([yup.ref('password')], 'Пароли не совпадают'),
});
