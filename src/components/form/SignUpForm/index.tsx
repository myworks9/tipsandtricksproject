/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';

// hooks
import { useForm } from 'react-hook-form';
import { useSignUp } from '../../../hooks';

// types
import { SignUpFormShapeType } from '../../../types';

// components
import { Input } from '../elements';

// other
import { schema } from './config';
import { book } from '../../../book';


export const SignUpForm: FC = () => {
    const mutation = useSignUp();

    const form = useForm<SignUpFormShapeType>({
        mode:     'onBlur',
        resolver: yupResolver(schema),
    });

    const submit = form.handleSubmit(async (
        userData: SignUpFormShapeType,
    ) => {
        const { confirmPassword, ...userRegisterData } = userData;
        await mutation.mutateAsync(userRegisterData);
        form.reset();
    });

    return (
        <section className = 'publish-tip'>
            <form onSubmit = { submit }>
                <fieldset disabled = { mutation.isLoading }>
                    <legend>Залогиниться</legend>
                    <Input
                        label = 'Имя и Фамилия'
                        register = { form.register('name') }
                        error = { form.formState.errors.name } />
                    <Input
                        label = 'Почта'
                        register = { form.register('email') }
                        error = { form.formState.errors.email } />
                    <Input
                        type = 'password'
                        label = 'Пароль'
                        register = { form.register('password') }
                        error = { form.formState.errors.password } />
                    <Input
                        type = 'password'
                        label = 'Подтверждение пароля'
                        register = { form.register('confirmPassword') }
                        error = { form.formState.errors.confirmPassword } />
                    <input
                        type = 'submit'
                        value = 'Зарегистрироваться' />
                </fieldset>
                <p>Перейти к <Link to = { book.login }> логину</Link>.
                </p>
            </form>
        </section>
    );
};
