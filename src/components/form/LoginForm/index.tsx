/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';

// hooks
import { useForm } from 'react-hook-form';
import { useLogin } from '../../../hooks';

// types
import { LoginFormRequestType } from '../../../types';

// components
import { Input } from '../elements';

// other
import { schema } from './config';
import { book } from '../../../book';

export const LoginForm: FC = () => {
    const mutation = useLogin();

    const form = useForm<LoginFormRequestType>({
        mode:     'onBlur',
        resolver: yupResolver(schema),
    });

    const submit = form.handleSubmit(async (credentials: LoginFormRequestType) => {
        await mutation.mutateAsync(credentials);
        form.reset();
    });

    return (
        <section className = ' publish-tip '>
            <form onSubmit = { submit }>
                <fieldset>
                    <legend>Залогиниться</legend>
                    <Input
                        type = 'email'
                        label = 'Почта'
                        register = { form.register('email') }
                        error = { form.formState.errors.email } />
                    <Input
                        type = 'password'
                        label = 'Пароль'
                        register = { form.register('password') }
                        error = { form.formState.errors.password } />
                    <input type = 'submit' value = 'Войти' />
                </fieldset>
                <p>
                    Если у вас до сих пор нет учётной записи, вы можете
                    <Link to = { book.signUp }> зарегистрироваться</Link>.
                </p>
            </form>
        </section>
    );
};

