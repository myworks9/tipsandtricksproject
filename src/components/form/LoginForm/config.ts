
// core
import * as yup from 'yup';

// types
import { LoginFormRequestType } from '../../../types';

// eslint-disable-next-line no-template-curly-in-string
const minLength = 'Минимальная длина ${min}';

// eslint-disable-next-line no-template-curly-in-string
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<LoginFormRequestType> = yup.object().shape({
    email:    yup.string().required('*').email('Введите email корректный'),
    password: yup.string().required('*').min(5, minLength).max(12, maxLength),
});
