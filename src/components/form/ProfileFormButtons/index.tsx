// core
import { FC } from 'react';

// hooks
import { useNavigate } from 'react-router-dom';

type PropTypes = {
    isDisabled: boolean
    isValid: boolean
    navigateTo?: {
        prev?: string,
        next?: string
    }
    submit: () => void
};

export const ProfileFormButtons: FC<PropTypes> = ({
    isDisabled, isValid, navigateTo, submit,
}) => {
    const navigate = useNavigate();

    const navigateToPrevHandler = () => {
        return navigateTo.prev ? navigate(navigateTo.prev) : null;
    };

    const navigateToNextHandler = () => {
        submit();

        return navigateTo.next ? navigate(navigateTo.next) : null;
    };

    return (
        <nav>
            <button
                onClick = { navigateToPrevHandler }
                disabled = { isDisabled }
                type = 'button'
                data-direction = 'back'> ← Назад
            </button>
            <button
                onClick = { navigateToNextHandler }
                disabled = { !isValid }
                type = 'button'
                data-direction = 'forward'> Дальше →
            </button>
        </nav>
    );
};
