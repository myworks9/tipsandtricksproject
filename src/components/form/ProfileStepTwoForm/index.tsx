/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector } from '../../../lib/redux/init/store';

// actions & selectors
import { authActions } from '../../../lib/redux/actions';
import { userProfileDataSelector } from '../../../lib/redux/selectors';

// types
import { ProfileStepTwoRequestType } from '../../../types';

// components
import { Input } from '../elements';
import { ProfileFormButtons } from '../ProfileFormButtons';
import { ProfilePage } from '../../../pages';

// schema
import { schema } from './config';
import { book } from '../../../book';


export const ProfileStepTwoForm: FC = () => {
    const dispatch = useAppDispatch();
    const userProfileData = useAppSelector(userProfileDataSelector);

    const { register, formState, getValues } = useForm<ProfileStepTwoRequestType>({
        mode:          'onChange',
        resolver:      yupResolver(schema),
        defaultValues: {
            job:      userProfileData.job || '',
            linkedin: userProfileData.linkedin || '',
        },
    });

    const submit = () => {
        const values = getValues();
        dispatch(authActions.setProfileData(values));
    };

    return (
        <ProfilePage >
            <form>
                <fieldset>
                    <legend>Должность и социальный профиль</legend>
                    <Input
                        register = { register('job') }
                        label = 'Род деятельности'
                        error = { formState.errors.job } />
                    <Input
                        register = { register('linkedin') }
                        label = 'Linkedin'
                        error = { formState.errors.linkedin } />
                    <ProfileFormButtons
                        submit = { submit }
                        navigateTo = { { prev: book.profileStepOne, next: book.profileStepThree } }
                        isDisabled = { false }
                        isValid = { formState.isValid } />
                </fieldset>
            </form>
        </ProfilePage>
    );
};
