// core
import * as yup from 'yup';

// types
import { ProfileStepTwoRequestType } from '../../../types';

export const schema: yup.SchemaOf<ProfileStepTwoRequestType> = yup.object().shape({
    job:      yup.string().required('Введите место Вашей роботы'),
    linkedin: yup.string().required('Введите Ваш linkedin').url('Введите корректную ссылку'),
});
