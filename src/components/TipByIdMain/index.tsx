// core
import { FC } from 'react';

// helpers
import { fetchify } from '../../helpers';

type PropsTypes = {
    body: string;
    isFetched: boolean;
};

export const TipByIdMain: FC<PropsTypes> = ({ body, isFetched }) => {
    return <main>{ fetchify(isFetched, body) }</main>;
};

