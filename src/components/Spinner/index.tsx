// Core
import { FC } from 'react';
import { Triangle } from 'react-loader-spinner';

export const Spinner: FC = () => {
    return (
        <div className = 'spinner'>
            <Triangle
                color = '#FD0E35'
                height = { 60 }
                width = { 60 } />
        </div>
    );
};
