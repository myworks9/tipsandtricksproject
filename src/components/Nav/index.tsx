// core
import { FC } from 'react';

// hooks
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/store';

// selectors
import { authTokenSelector, isSettingsOpenSelector } from '../../lib/redux/selectors';

// mock
import { mockLinksData } from './mockLinksData';

// book
import { CustomLink } from './CustomLink';

export const Nav: FC = () => {
    const dispatch = useAppDispatch();
    const authToken = useAppSelector(authTokenSelector);
    const isSettingsOpen = useAppSelector(isSettingsOpenSelector);
    const mockLinks = mockLinksData(dispatch, authToken, isSettingsOpen);

    const linksJSX = mockLinks.map((link) => (
        <CustomLink
            key = { link.text }
            text = { link.text }
            disableLinkHandler = { link.disableLinkHandler }
            path = { link.path }
            openSettingsHandler = { link.openSettingsHandler }
            icon = { link.icon } />
    ));

    return (
        <nav className = 'nav'>
            <h1 title = 'Типсы и Триксы'>T и T</h1>
            { linksJSX }
        </nav>
    );
};
