// core
import { SyntheticEvent } from 'react';

// hooks
import { AppDispatch } from '../../lib/redux/init/store';

// actions
import { settingsActions } from '../../lib/redux/actions';

// types
import { CustomLinkTypes } from '../../types';

// helpers
import { icons } from '../../theme/icons/nav';
import { book } from '../../book';

export const mockLinksData = (
    dispatch: AppDispatch, authToken: string, isSettingsOpen: boolean,
): CustomLinkTypes[] => {
    const disableLinkHandler = (event: SyntheticEvent<HTMLAnchorElement>): void => {
        if (!authToken) {
            event.preventDefault();
        }
    };

    const openSettingsHandler = (
        event: SyntheticEvent<HTMLAnchorElement>,
    ): void => {
        event.preventDefault();
        if (!isSettingsOpen) {
            dispatch(settingsActions.setSettingsOpen(true));
        }
    };

    return [
        {
            path: book.allPages,
            disableLinkHandler,
            text: 'Все темы',
            icon: icons.Home,
        },
        {
            path: book.byTagPage,
            disableLinkHandler,
            text: 'По тэгам',
            icon: icons.Tag,
        },
        {
            path: book.publish,
            disableLinkHandler,
            text: 'Опубликовать',
            icon: icons.Publish,
        },
        {
            path: book[ '*' ],
            disableLinkHandler,
            openSettingsHandler,
            text: 'Настройки',
            icon: icons.Settings,
        },
        {
            path: book.profileStepOne,
            disableLinkHandler,
            text: 'Профиль',
            icon: icons.Profile,
        },
        {
            path: book.login,
            disableLinkHandler,
            text: 'Войти',
            icon: icons.Bolt,
        },
        {
            path: book.login,
            disableLinkHandler,
            text: 'Выйти',
            icon: icons.Logout,
        },
    ];
};
