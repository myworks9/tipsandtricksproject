// core
import { FC, SyntheticEvent } from 'react';
import { NavLink } from 'react-router-dom';
import cx from 'classnames';

// hooks
import { useAppSelector } from '../../lib/redux/init/store';
import { useLogOut } from '../../hooks';

// selectors
import { authTokenSelector, isSettingsOpenSelector } from '../../lib/redux/selectors';

// types
import { CustomLinkTypes } from '../../types';

export const CustomLink: FC<CustomLinkTypes> = (props) => {
    const authToken = useAppSelector(authTokenSelector);
    const { mutateAsync } = useLogOut();
    const isSettingsOpen = useAppSelector(isSettingsOpenSelector);
    const activeClassName = cx(authToken ? 'link-with-hover' : 'link');

    if (props.text === 'Войти' && authToken) {
        return null;
    }

    if (props.text === 'Выйти' && !authToken) {
        return null;
    }

    const navLinkHandler = (event: SyntheticEvent<HTMLAnchorElement>) : void => {
        props.disableLinkHandler(event);

        if (props.text === 'Выйти') {
            void mutateAsync();
        }
    };

    return (
        props.text === 'Настройки' ? (
            <a
                className = { `${activeClassName} ${isSettingsOpen && 'active'} ` }
                onClick = { authToken ? props.openSettingsHandler : props.disableLinkHandler }
                href = { props.path }> <props.icon /> { props.text }
            </a>
        ) : (
            <NavLink
                className = { activeClassName }
                onClick = { navLinkHandler }
                to = { props.path }>
                <props.icon /> { props.text }
            </NavLink>
        )
    );
};
