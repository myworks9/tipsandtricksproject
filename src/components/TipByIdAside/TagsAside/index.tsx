// core
import { FC } from 'react';

// hooks
import { useTags } from '../../../hooks';

// helpers
import { fetchify } from '../../../helpers';

// components
import { TagAside } from './TagAside';

export const TagsAside: FC = () => {
    const { tags, isFetched } = useTags();

    const tagsJSX =  tags.map((tag) => <TagAside key = { tag.id } tag = { tag } />);

    return (
        <aside className = 'tags-aside'>
            <h1>Тэги</h1>
            <div>
                { fetchify(isFetched, tagsJSX) }
            </div>
        </aside>
    );
};
