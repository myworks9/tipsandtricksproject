// core
import { FC } from 'react';
import { Link } from 'react-router-dom';

// hooks
import { useAppDispatch } from '../../../../lib/redux/init/store';

// actions
import { tagsAndTipsActions } from '../../../../lib/redux/actions';

// types
import { TagModelResponseType } from '../../../../types';

// helpers
import { getTagIcon } from '../../../../helpers';

type PropsTypes = {
    tag: TagModelResponseType;
};

export const TagAside: FC<PropsTypes> = ({ tag }) => {
    const dispatch = useAppDispatch();
    const TagIcon = getTagIcon(tag.name);

    const setActiveTagInfoHandler = () => {
        dispatch(tagsAndTipsActions.setSelectedTag(tag));
    };

    return (
        <span className = 'tag'>
            <Link onClick = { setActiveTagInfoHandler } to = { '/by-tag-page/' }>
                <TagIcon /> { tag.name }
            </Link>
        </span>
    );
};

