// core
import { FC } from 'react';

// types
import { ViewPage } from '../../types';

// components
import { RecentTips } from './RecentTips';
import { TagsAside } from './TagsAside';

type PropType = {
    viewPage: ViewPage
};

export const TipByIdAside: FC<PropType> = ({ viewPage }) => {
    return (
        <section className = 'asides'>
            <RecentTips viewPage = { viewPage } />
            <TagsAside />
        </section>
    );
};
