/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { Link } from 'react-router-dom';
import { FC } from 'react';

// types
import { TipModelResponseType } from '../../../../types';

// helpers
import { formatDate, getTagIcon } from '../../../../helpers';

type PropsTypes = {
    tip: TipModelResponseType
};

export const RecentTip: FC<PropsTypes> = ({ tip }) => {
    const TagImg = getTagIcon(tip.tag.name);

    return (
        <Link
            to = { `../${tip.id}` }>
            <h3>{ tip.title }</h3>
            <time>
                <TagImg />
                { formatDate(tip.created) }
            </time>
        </Link>
    );
};

