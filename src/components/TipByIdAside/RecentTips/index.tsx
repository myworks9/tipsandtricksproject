// core
import { FC } from 'react';

// hooks
import { useTips } from '../../../hooks';

// types
import { ViewPage } from '../../../types';

// helpers
import { sortByDate, fetchify } from '../../../helpers';

// components
import { RecentTip } from './RecentTip';

type PropType = {
    viewPage: ViewPage
};

export const RecentTips: FC<PropType> = ({ viewPage }) => {
    const { tips, isFetched } = useTips(viewPage);

    const recentJSX = tips
        ?.sort(sortByDate)
        ?.slice(0, 5)
        ?.map((tip) => {
            return (
                <RecentTip
                    key = { tip.id }
                    tip = { tip } />
            );
        });

    return (
        <aside className = 'recent-tips'>
            <h1>Свежие типсы</h1>
            { fetchify(isFetched, recentJSX) }
        </aside>
    );
};

