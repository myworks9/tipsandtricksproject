// core
import { FC } from 'react';
import { Link } from 'react-router-dom';

// helpers
import { fetchify, formatDate, getTagIcon } from '../../helpers';

type PropsTypes = {
    tagImg: string;
    title: string;
    created: string;
    author: string;
    isFetched: boolean;
};

export const TipByIdHero: FC<PropsTypes> = ({
    tagImg, title, created, author, isFetched,
}) => {
    const TagIcon = getTagIcon(tagImg);

    return (
        <article>
            <header>
                <TagIcon />
                <h1>{ fetchify(isFetched, title) }</h1>
            </header>
            <main>
                <time>
                    <div>
                        <span>
                            🚀 { fetchify(isFetched, formatDate(created)) }
                        </span>
                        <span>👨🏼‍🚀 Автор: { fetchify(isFetched, author) }</span>
                    </div>
                </time>
            </main>
            <footer>
                <Link to = '..'>← Назад</Link>
            </footer>
        </article>
    );
};

