// core
import { FC } from 'react';
import cx from 'classnames';

// hooks
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/store';

// selectors && actions
import { settingsActions } from '../../lib/redux/actions';
import { isSettingsOpenSelector } from '../../lib/redux/selectors';

export const Settings: FC = () => {
    const dispatch = useAppDispatch();
    const isSettingsOpen = useAppSelector(isSettingsOpenSelector);

    const closeSettingsHandler = () => {
        dispatch(settingsActions.setSettingsOpen(false));
    };

    const settingsClose = cx('settings', {
        open:   isSettingsOpen,
        closed: !isSettingsOpen,
    });

    return (
        <section className = { settingsClose }>
            <header>
                <h1>Настройки</h1>
            </header>
            <footer>
                <button onClick = { closeSettingsHandler }>Закрыть</button>
            </footer>
        </section>
    );
};
