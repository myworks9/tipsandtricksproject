/* eslint-disable @typescript-eslint/no-misused-promises */
// core
import { FC } from 'react';
import { Link } from 'react-router-dom';

// hooks
import { usePrefetching } from '../../../hooks';

// types
import { TipModelResponseType } from '../../../types';

// helper
import { formatDate, getTagIcon } from '../../../helpers';

type PropsType = {
    tip: TipModelResponseType
};

export const Tip: FC<PropsType> = ({ tip }) => {
    const prefetching = usePrefetching(['tip', tip.id]);

    const TagIconJSX = getTagIcon(tip.tag.name);

    return (
        <article>
            <header>
                <TagIconJSX /> { tip.title }
            </header>
            <main>
                <time>
                    <TagIconJSX />
                    <div>
                        <span>🚀 { formatDate(tip.created) }</span>
                        <span>👨🏼‍🚀 Автор: { tip.author }</span>
                    </div>
                </time>
                <h2>{ tip.title }</h2>
                <p>{ tip.preview }</p>
            </main>
            <footer>
                <Link
                    onMouseEnter = { prefetching }
                    to = { tip.id }>📖&nbsp;Читать полностью &rarr;
                </Link>
            </footer>
        </article>
    );
};
