// core
import { FC } from 'react';

import { useTips } from '../../hooks';

// types
import { ViewPage } from '../../types';

// helpers
import { fetchify } from '../../helpers';

// components
import { Tip } from './Tip';
import { Spinner } from '../Spinner';

type PropsTypes = {
    viewPage: ViewPage
};

export const TipList: FC<PropsTypes> = ({ viewPage }) => {
    const { tips, isFetched, isFetching } = useTips(viewPage);

    const tipsJSX = tips.map((tip) => {
        return <Tip key = { tip?.id } tip = { tip } />;
    });

    return (
        <section className = 'tip-list'>
            { fetchify(isFetched, tipsJSX)  }
            { isFetching && <Spinner /> }
        </section>
    );
};
