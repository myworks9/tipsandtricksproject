// core
import { FC } from 'react';

// types
import { ViewPage } from '../../types';

// components
import { Tags } from './Tags';

type PropsTypes = {
    viewPage: ViewPage
    children: React.ReactNode
};

export const Hero: FC<PropsTypes> = ({ viewPage, children }) => {
    return (
        <section className = 'hero'>
            <div className = 'title'>
                <h1>Типсы и Триксы</h1>
                { children }
            </div>
            <Tags viewPage = { viewPage } />
        </section>
    );
};
