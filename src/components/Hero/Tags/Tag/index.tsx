// core
import { FC } from 'react';

// hooks
import { useAppDispatch } from '../../../../lib/redux/init/store';

// actions
import { tagsAndTipsActions } from '../../../../lib/redux/actions';

// types
import { TagModelResponseType, ViewPage } from '../../../../types';

// helpers
import { getTagIcon } from '../../../../helpers/getTagIcon';

type PropsTypes = {
    viewPage: ViewPage;
    tag: TagModelResponseType;
    selectedTagInfo: TagModelResponseType;
};

export const Tag: FC<PropsTypes> = ({ tag, viewPage, selectedTagInfo }) => {
    const dispatch = useAppDispatch();

    const TagIcon = getTagIcon(tag.name);

    const setSelectedTagHandler = () => {
        dispatch(tagsAndTipsActions.setSelectedTag(tag));
    };

    return (
        <span
            onClick = { viewPage !== ViewPage.ALL_TIPS ? setSelectedTagHandler : null }
            className = 'tag'
            data-active = {
                selectedTagInfo?.id === tag?.id || viewPage === ViewPage.ALL_TIPS
            }>
            <TagIcon /> { tag.name }
        </span>
    );
};

