// core
import { FC } from 'react';

// hooks
import { useTags } from '../../../hooks';

// types
import { ViewPage } from '../../../types';

// components
import { Tag } from './Tag';

// helpers
import { fetchify } from '../../../helpers';


type PropTypes = {
    viewPage: ViewPage
};

export const Tags: FC<PropTypes> = ({ viewPage }) => {
    const { tags, isFetched, selectedTagInfo } = useTags();

    const tagsJSX = tags.map((tag) => {
        return <Tag
            selectedTagInfo = { selectedTagInfo }
            key = { tag.id }
            viewPage = { viewPage }
            tag = { tag } />;
    });

    return  <div className = 'tags'> { fetchify(isFetched, tagsJSX) } </div>;
};

