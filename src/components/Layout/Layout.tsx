// core
import { FC, ReactNode } from 'react';

// hooks
import { useCheckToken } from '../../hooks';

// components
import { Nav } from '../Nav';

type PropTypes = {
    children: ReactNode
};

export const Layout: FC<PropTypes> = ({ children }) => {
    useCheckToken();

    return (
        <section className = 'layout'>
            <Nav />
            { children }
        </section>
    );
};
