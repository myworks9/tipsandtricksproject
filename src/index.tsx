// core
import { render } from 'react-dom';
import { QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { Slide, ToastContainer } from 'react-toastify';
import { PersistGate } from 'redux-persist/integration/react';
import { queryClient } from './lib/queryClient';

// store
import { persistor, store } from './lib/redux/init/store';

// Components
import { App } from './App';
import { Spinner } from './components';

// Instruments
import 'react-toastify/dist/ReactToastify.css';
import './theme/main.scss';

render(
    <Provider store = { store }>
        <PersistGate loading = { <Spinner /> } persistor = { persistor }>
            <QueryClientProvider client = { queryClient }>
                <Router>
                    <ToastContainer newestOnTop transition = { Slide } />
                    <App />
                </Router>
                <ReactQueryDevtools initialIsOpen = { false } />
            </QueryClientProvider>
        </PersistGate>
    </Provider>,
    document.getElementById('root'),
);
