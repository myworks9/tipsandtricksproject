// core
import { FC } from 'react';
import {
    Navigate, Outlet, Route, Routes,
} from 'react-router-dom';

// hooks
import { useInterceptors, useScrollToTop, useToastError } from './hooks';

// components
import {
    Layout, ProfileStepOneForm, ProfileStepThreeForm, ProfileStepTwoForm, Settings,
} from './components';
import {
    AllPages,
    ByTagPage,
    LoginPage,
    PublishTipPage,
    SignUpPage,
    TipById,
} from './pages';

// book
import { book } from './book';

export const App: FC = () => {
    useInterceptors();
    useToastError();
    useScrollToTop();

    return (
        <>
            <Settings />
            <Layout>
                <Routes>
                    <Route path = { book.allPages } element = { <Outlet /> }>
                        <Route path = { book[ '/' ] } element = { <AllPages /> } />
                        <Route path = { book.tipById } element = { <TipById /> } />
                    </Route>

                    <Route path = { book.byTagPage } element = { <Outlet /> }>
                        <Route path = { book[ '/' ] } element = { <ByTagPage /> } />
                        <Route path = { book.tipById } element = { <TipById /> } />
                    </Route>

                    <Route path = { book.publish } element = { <PublishTipPage /> } />
                    <Route path = { book.login } element = { <LoginPage /> } />
                    <Route path = { book.signUp } element = { <SignUpPage /> } />
                    <Route path = { book.profileStepOne } element = { <ProfileStepOneForm /> } />
                    <Route path = { book.profileStepTwo } element = { <ProfileStepTwoForm /> } />
                    <Route
                        path = { book.profileStepThree }
                        element = { <ProfileStepThreeForm /> } />

                    <Route
                        path = { book[ '*' ] } element = {
                            <Navigate to = { book.allPages } replace /> } />
                </Routes>
            </Layout>
        </>
    );
};
