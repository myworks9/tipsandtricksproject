// constants
import { toastConstants } from './toastConstants';

type ToastMessagesTypes = {
    name: string;
    payload?: string;
};

type ToastMessageType = (message: ToastMessagesTypes) => string;

export const toastMessages: ToastMessageType = (message) => {
    switch (message.name) {
        case toastConstants.SUCCESS_AUTHORIZATION:
            return `Добро пожаловать ${message.payload}`;
        case toastConstants.CHANGE_PROFILE_DATA:
            return 'Так как API пока не поддерживает эту функцию, просто знайте, что Ваши данные были б успешно обновлены!)';
        default:
            return 'Не предусмотренное сообщение, но все равно поздравляем!';
    }
};
