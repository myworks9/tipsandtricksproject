// core
import { ToastPosition } from 'react-toastify';

type ToastOptionsType = {
    position:        ToastPosition,
    autoClose:       number,
    hideProgressBar: boolean,
    closeOnClick:    boolean,
    pauseOnHover:    boolean,
    draggable:       boolean,
    progress:        undefined,
};

export const toastOptions: ToastOptionsType = {
    position:        'top-right',
    autoClose:       5000,
    hideProgressBar: false,
    closeOnClick:    true,
    pauseOnHover:    true,
    draggable:       true,
    progress:        undefined,
} as const;
