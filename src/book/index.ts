type BookType = {
    '/': string
    '*': string
    login: string
    signUp: string
    byTagPage: string
    allPages: string
    tipById: string
    publish: string
    profileStepOne: string
    profileStepTwo: string
    profileStepThree: string
};

export const book: BookType = {
    '/':              '/',
    '*':              '*',
    login:            '/login',
    signUp:           '/sign-up',
    byTagPage:        '/by-tag-page',
    allPages:         '/all-pages',
    tipById:          '/:id',
    publish:          '/publish',
    profileStepOne:   '/profile/step-1',
    profileStepTwo:   '/profile/step-2',
    profileStepThree: '/profile/step-3',
} as const;
